package com.escalade.oc.escalade.escaladeocsite.controller;

import com.escalade.oc.escalade.escaladeocsite.model.User;
import com.escalade.oc.escalade.escaladeocsite.service.IUserService;
import com.escalade.oc.escalade.escaladeocsite.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
public class UserController {
    @Autowired
    private IUserService IUserService;

    @Autowired
    private UserValidator userValidator;


    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public String registration(Model model) {
        model.addAttribute("userForm", new User());

        return "compte";
    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public String registration(@ModelAttribute("userForm") User userForm, BindingResult bindingResult, Model model) {
        userValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return "compte";
        }

        IUserService.save(userForm);

        return "redirect:/compte";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Your username and password is invalid.");
        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");

        return "login";
    }

    @RequestMapping(value = { "/compte"}, method = RequestMethod.GET)
    public String compte(Model model) {
        return "compte";
    }


    @GetMapping("/")
    public String root(){
        return "header";
    }

    @GetMapping("/compte")
    public String compteroot(){
        return "compte/header";
    }
}
