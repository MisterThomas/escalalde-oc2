package com.escalade.oc.escalade.escaladeocsite.service;

import com.escalade.oc.escalade.escaladeocsite.model.Article;
import com.escalade.oc.escalade.escaladeocsite.model.User;
import com.escalade.oc.escalade.escaladeocsite.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
@Service
public class ArticleService implements IArticleService {


    @Autowired
    private ArticleRepository articleRepository;


    @Override
    public List<User> getArticlesByUser(String user) {
        return articleRepository.findByUserName(user);
    }

    @Override
    public Optional<User> getArticleById(long id) {
        return articleRepository.findById(id);
    }

    @Override
    public void updateArticle(Article article) {
        // articleRepository.save(article);
    }


    @Override
    public void addArticle(String name, String desc, String texte, Date targetDate, boolean isDone) {

    }

    @Override
    public void deleteArticle(long id) {
        Optional<User> article = articleRepository.findById(id);
        if (article.isPresent()) {
            articleRepository.delete(article.get());
        }
    }

    @Override
    public void saveArticle(Article article) {
        //articleRepository.save(article);
    }

}
