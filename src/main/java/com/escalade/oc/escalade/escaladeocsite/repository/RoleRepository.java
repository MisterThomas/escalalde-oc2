package com.escalade.oc.escalade.escaladeocsite.repository;

import com.escalade.oc.escalade.escaladeocsite.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
