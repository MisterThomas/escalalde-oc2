package com.escalade.oc.escalade.escaladeocsite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EscaladeOcSiteApplication {

	public static void main(String[] args) {
		SpringApplication.run(EscaladeOcSiteApplication.class, args);
	}

}
