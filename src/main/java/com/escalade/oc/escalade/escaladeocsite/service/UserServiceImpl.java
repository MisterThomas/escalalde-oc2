package com.escalade.oc.escalade.escaladeocsite.service;


import com.escalade.oc.escalade.escaladeocsite.model.User;
import com.escalade.oc.escalade.escaladeocsite.repository.RoleRepository;
import com.escalade.oc.escalade.escaladeocsite.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements IUserService {


    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;


    @Override
    public void save(User user) {

    }

    @Override
    public User findByEmail(String email) {
        return null;
    }
}
