package com.escalade.oc.escalade.escaladeocsite.model;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Table(name = "article")
public class Article {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;


    private String userName;


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @NotNull
    @Size(min = 3, message = "Entrer 3 charactere de plus ...")
    private String description;


    @Size(min = 10, message = "Enter plus de 10 characteres...")
    private String texte;

    private Date targetDate;


    public Article(){
        super();
    }


    public Article(String userName, @NotNull @Size(min = 3, message = "Entrer 3 charactere de plus ...") String description, @Size(min = 10, message = "Enter plus de 10 characteres...") String texte, Date targetDate, boolean isDone) {
        this.userName = userName;
        this.description = description;
        this.texte = texte;
        this.targetDate = targetDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTexte() {
        return texte;
    }

    public void setTexte(String texte) {
        this.texte = texte;
    }

    public Date getTargetDate() {
        return targetDate;
    }

    public void setTargetDate(Date targetDate) {
        this.targetDate = targetDate;
    }
}
