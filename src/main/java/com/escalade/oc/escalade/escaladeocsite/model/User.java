package com.escalade.oc.escalade.escaladeocsite.model;



import javax.persistence.*;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

public class User {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "utilisateur_id_pk")
    private Long id;
   @Column(name = "username")
    private String username;
    @Column(name = "password")
    private String password;
    @Column(name = "password_confirm")
    private String passwordConfirm;

    @NotNull
    // @Size(min = 1, max = 30)
    @Column(name = "nom")
    private String nom;

    @NotNull
    //@Size(min = 1, max = 30)
    @Column(name = "prenom")
    private String prenom;


    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "mail")
    private String email;


    @NotNull
    @Size(min = 10, max = 10)
    @Column(name = "numero_telephone")
    private String numero_telephone;


    @AssertTrue
    private Boolean terms;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNumero_telephone() {
        return numero_telephone;
    }

    public void setNumero_telephone(String numero_telephone) {
        this.numero_telephone = numero_telephone;
    }

    @Column(name = "role")
    private Set < Role > roles;


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
/*
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
*/
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
}
