package com.escalade.oc.escalade.escaladeocsite.service;

import com.escalade.oc.escalade.escaladeocsite.model.User;

public interface IUserService {
    void save(User user);

    User findByEmail(String email);
}
