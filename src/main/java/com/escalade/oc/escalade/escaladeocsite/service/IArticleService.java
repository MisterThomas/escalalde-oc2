package com.escalade.oc.escalade.escaladeocsite.service;

import com.escalade.oc.escalade.escaladeocsite.model.Article;
import com.escalade.oc.escalade.escaladeocsite.model.User;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface IArticleService {

    List<User> getArticlesByUser(String user);

    Optional<User> getArticleById(long id);

    void updateArticle(Article article);

    void addArticle(String name, String desc,String texte, Date targetDate, boolean isDone);

    void deleteArticle(long id);

    void saveArticle(Article article);
}
