package com.escalade.oc.escalade.escaladeocsite.controller;


import com.escalade.oc.escalade.escaladeocsite.model.Article;
import com.escalade.oc.escalade.escaladeocsite.model.User;
import com.escalade.oc.escalade.escaladeocsite.service.IArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class ArticleController {

    @Autowired
    private IArticleService articleService;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        // Date - dd/MM/yyyy
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    }

    @RequestMapping(value = "/list-article", method = RequestMethod.GET)
    public String showArticles(ModelMap model) {
        String name = getLoggedInUserName(model);
        model.put("articles", articleService.getArticlesByUser(name));
        // model.put("todos", service.retrieveTodos(name));
        return "list-article";
    }

    private String getLoggedInUserName(ModelMap model) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            return ((UserDetails) principal).getUsername();
        }

        return principal.toString();
    }

    @RequestMapping(value = "/add-article", method = RequestMethod.GET)
    public String showAddArticlePage(ModelMap model) {
        model.addAttribute("article", new Article());
        return "article";
    }

    @RequestMapping(value = "/delete-article", method = RequestMethod.GET)
    public String deleteArticle(@RequestParam long id) {
        articleService.deleteArticle(id);
        // service.deleteTodo(id);
        return "redirect:/list-article";
    }

    @RequestMapping(value = "/update-article", method = RequestMethod.GET)
    public String showUpdateArticlePage(@RequestParam long id, ModelMap model) {
        // Article User
        User article = articleService.getArticleById(id).get();
        model.put("article", article);
        return "todo";
    }

    @RequestMapping(value = "/update-article", method = RequestMethod.POST)
    public String updateArticle(ModelMap model, @Valid Article article, BindingResult result) {

        if (result.hasErrors()) {
            return "todo";
        }

        article.setUserName(getLoggedInUserName(model));
        articleService.updateArticle(article);
        return "redirect:/list-todos";
    }

    @RequestMapping(value = "/add-article", method = RequestMethod.POST)
    public String addArticle(ModelMap model, @Valid Article article, BindingResult result) {

        if (result.hasErrors()) {
            return "article";
        }

        article.setUserName(getLoggedInUserName(model));
        articleService.saveArticle(article);
        return "redirect:/list-todos";
    }

}
