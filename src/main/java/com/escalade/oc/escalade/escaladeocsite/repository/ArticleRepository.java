package com.escalade.oc.escalade.escaladeocsite.repository;

import com.escalade.oc.escalade.escaladeocsite.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ArticleRepository extends JpaRepository<User, Long > {
    List<User> findByUserName(String user);
}
